6-digit Counter Semantic Turkey URIGenerator
============================================

This repository implements a `URIGenerator` for [Semantic Turkey](http://semanticturkey.uniroma2.it/) that
uses a 6-digit counter. This software was developed to meet specific requirements emerged during a [VocBench](http://vocbench.uniroma2.it/) adoption story. While this software was released under an open source license,
usage by other parties will not officially supported. 

Assumptions
-----------

This implementation assumes a usage scenario characterized by
* low-contention: few concurrent requests
* low-demand sporadic requests

Characteristics
---------------

This implementation has the following characteristics:
* six-digit counter, possibly padded with zeroes on the left
* protection against accidental generation of identifiers already used in the target repository
* requires manual setup on first use
* requires manual reconfiguration in case of errors

Requirements
------------

This implementation requires version 13.x of Semantic Turkey.

Installation
------------

It is necessary that Semantic Turkey is launched without the `clean` option, and then copy the main artifact
`sixdigit-counter-st-urigen-${project.version}.jar` inside the `plugins` directory of the
Semantic Turkey installation.

Usage inside VocBench
---------------------

*Project setup*

The URI Generator for a project must be configured upon the creation of the project and, currently, it is not
possible to reconfigure it later.

After installing this _6-digit Counter URIGenerator_, you can configure it as follows inside the window for the
creation of a new project:

 *  open the panel _Optional Settings_, by clicking on the triangle near the label of the panel
 * locate the subsection titled _URI Generator_ and uncheck the option _use default settings_
 * a few field in the subsection should become enabled
 * use the combox labeled _Plugin_ to select `6DigitCounterLegacyURIGeneratorFactory`
 * that's all... you can specify the rest of the configuration (project name, base URI, ...) and click the
  _Create_ button


*First Use*

The URI Generator is used to produce a URI, when one is not passed by the user. To observe this
component in action, it is possible to create a SKOS Concept or Concept Scheme without indicating a URI.

When this URI Generator is first used, VocBench will produce an error like this:

> Project settings for the URI Generator are not valid: property: counter2nextIdentifier has not been set

As stated in the characteristics, this URI Generator does not attempt to guess the first usable identifier, but
instead relies on manual configuration.

To configure the URI Generator, you must open (or create if not existing) the file:

	<SemanticTurkeyData>/projects/<ProjectName>/plugins/org.bitbucket.manuelfiorelli.semanticturkey.sixdigitcounterurigen.SixDigitCounterURIGenerator/settings.props

where <SemanticTurkeyData> denotes the directory the Semantic Turkey stores its data, while <ProjectName> is
the name of the project for which you want to configure the URI Generator.

Place inside the file a text like the following:

	counter2nextIdentifier:
	  concept: 1
	  conceptScheme: 2
	  xLabel: 3
	  default: 4
	templates:
	  concept: c_${seq(concept)}
	  conceptScheme: conceptScheme_${seq(conceptScheme)}
	  xLabel: xl_${lexicalForm.language}_${seq(xLabel)}
	  fallback: ${xRole}_${seq()}
	
The field `counter2nextIdentifier` defines a number of named counters (in the example, `default`,
`concept`, `conceptScheme` and `xLabel`), each one associated with a next numeric identifier. The
counter `default` is considered when a numeric identifier is requested but no counter name is provided
explicitly.

The field `templates` eXtnded Roles (xRoles) with the template for the instantiation of local names, which
are then append to the default namespace to obtain an IRI. The virtual xRole `fallback` is used to indicate
the template that is considered if no other template is relevant. Please note that each association is given
on a line that should be indented with two spaces. Furthermore, if `fallback` is not explicitly mapped to a
template, then there would be really no fallback: if no template matches the requested xRole, then the
generation fails.

In the example configuration concepts and xLabels uses different counters that can potentially have the same
value. However, there can be no collision because they use different prefixes, respectively, `c_` and `xl_`.
	
Obviously, it is necessary to choose templates, counters and their values that meet the specific use case: in
particular, given a prefix, it is necessary that the counter has a value greater than the maximum identifier
already in use in the dataset being edited.

*Note* in a real-word scenario, you should edit this file when Semantic Turkey is down, so that it is not
possible that the URI Generator attempts to read its settings, when they are being edited.

Design decisions and limitations
--------------------------------

This URI Generator was implemented following the new extension framework being developed for Semantic Tukey
4.0; however, the fact that Semantic Turkey still relies on the legacy approach to develop its URI Generators,
a compatibility layer was introduced.

The number telling the next identifier is stored in a textual file, using the API offered by Semantic Turkey
to its extensions. This choice had the following consequences:
* limited durability of the configuration, in the sense that a failure of the system may corrupt the
  configuration of the URI Generator
* no protection against concurrent attempts to modify the configuration

Regarding the second problem, this implementation serializes the accesses originating from the URI Generator,
so that they do not happen concurrently. There is, however, a chance that other parts of the system (e.g. the
user through the settings-related services) concurrently modify the configuration files.

This limitation could be addressed in two manners:
* the URI Generator identifies the problem and then complains about it
* the settings API itself is made more resilient with respect to concurrent accesses

It is worth to mention the fact that the generation of URIs usually occurs in the context of a transaction
within the service layer of ST; however, if a transaction is aborted for whatever reason, the state of a URI
Generator is not reverted to its previous value, i.e. once incremented, a counter is never decremented as a
consequence of an exception.