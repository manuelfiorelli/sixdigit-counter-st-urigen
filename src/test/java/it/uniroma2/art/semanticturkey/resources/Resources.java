/*
 * Copyright (C) 2018 Manuel Fiorelli <mailto:manuel.fiorelli@gmail.com>
 * All rights reserved.
 * This software may be modified and distributed under the terms of the BSD-3-Clause license.
 * See the LICENSE.txt file for details.
 */
package it.uniroma2.art.semanticturkey.resources;

import java.io.File;
import java.util.Objects;

/**
 * Overrides {@link Resources} in {@code st-core-framework} to support testing.
 * 
 * @author <a href="mailto:manuel.fiorelli@gmail.com">Manuel Fiorelli</a>
 *
 */
public class Resources {

	private static final String _projectsDirName = "projects";
	private static final String _systemDirName = "system";
	private static final String _usersDirName = "users";
	private static final String _projectUserBindingsDirName = "pu_bindings";

	private static File stDataDirectory;
	private static File projectsDir;
	private static File systemDir;
	private static File usersDir;
	private static File projectUserBindingsDir;

	public static void setSemTurkeyDataDir(File dir) {
		stDataDirectory = dir;
		projectsDir = new File(dir, _projectsDirName);
		systemDir = new File(dir, _systemDirName);
		usersDir = new File(dir, _usersDirName);
		projectUserBindingsDir = new File(dir, _projectUserBindingsDirName);
	}

	public static File getSemTurkeyDataDir() {
		return Objects.requireNonNull(stDataDirectory);
	}

	public static File getProjectsDir() {
		return Objects.requireNonNull(projectsDir);
	}

	public static File getSystemDir() {
		return Objects.requireNonNull(systemDir);
	}

	public static File getUsersDir() {
		return Objects.requireNonNull(usersDir);
	}

	public static File getProjectUserBindingsDir() {
		return Objects.requireNonNull(projectUserBindingsDir);
	}

}
