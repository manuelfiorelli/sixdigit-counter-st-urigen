/*
 * Copyright (C) 2018 Manuel Fiorelli <mailto:manuel.fiorelli@gmail.com>
 * All rights reserved.
 * This software may be modified and distributed under the terms of the BSD-3-Clause license.
 * See the LICENSE.txt file for details.
 */
package org.bitbucket.manuelfiorelli.semanticturkey.sixdigitcounterurigen;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import org.apache.commons.io.FileUtils;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Namespace;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.impl.SimpleNamespace;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.model.vocabulary.SKOS;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.repository.util.Repositories;
import org.eclipse.rdf4j.sail.memory.MemoryStore;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import com.google.common.collect.Sets;

import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.stringContainsInOrder;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import it.uniroma2.art.semanticturkey.extension.extpts.urigen.URIGenerationException;
import it.uniroma2.art.semanticturkey.extension.extpts.urigen.URIGenerator;
import it.uniroma2.art.semanticturkey.project.Project;
import it.uniroma2.art.semanticturkey.properties.STPropertyAccessException;
import it.uniroma2.art.semanticturkey.properties.STPropertyUpdateException;
import it.uniroma2.art.semanticturkey.resources.Resources;
import it.uniroma2.art.semanticturkey.services.STServiceContext;

import it.uniroma2.art.semanticturkey.extension.settings.impl.SettingsSupport;


/**
 * Test cases for {@link SixDigitCounterURIGenerator}
 * 
 * @author <a href="mailto:manuel.fiorelli@gmail.com">Manuel Fiorelli</a>
 *
 */
public class SixDigitCounterURIGeneratorTest {

	static final String BASEURI = "http://example.org/";
	static final String NAMESPACE = "http://example.org/";
	static final Namespace NS = new SimpleNamespace("example", NAMESPACE);
	static final String SEMANTIC_TURKEY_DIR_PATH = "target/tests-base/STData";
	static final String TEST_PROJECT_NAME = "TestProject";

	Project project;
	STServiceContext stServiceContext;

	Repository repository;
	RepositoryConnection connection;

	SixDigitCounterURIGenerator urigen;
	SixDigitCounterURIGeneratorFactory factory;

	static IRI iri(Namespace ns, String localName) {
		return SimpleValueFactory.getInstance().createIRI(ns.getName(), localName);
	}

	@BeforeEach
	void init() throws IOException {

		ApplicationEventPublisher applicationEventPublisher = mock(ApplicationEventPublisher.class);
		doAnswer(invocationOnMock -> null).when(applicationEventPublisher).publishEvent(any());

		SettingsSupport.setEventPublisher(applicationEventPublisher);
		
		repository = new SailRepository(new MemoryStore());
		repository.init();

		connection = repository.getConnection();
		connection.setNamespace("", NAMESPACE);
		TransactionSynchronizationManager.bindResource(repository, connection);

		project = mock(Project.class);
		stServiceContext = mock(STServiceContext.class);

		when(project.getRepository()).thenReturn(repository);
		when(stServiceContext.getProject()).thenReturn(project);
		when(project.getName()).thenReturn(TEST_PROJECT_NAME);

		// Creates a fresh new STData directory
		File stDataDir = new File(SEMANTIC_TURKEY_DIR_PATH);
		stDataDir.mkdirs();
		FileUtils.cleanDirectory(stDataDir);

		Resources.setSemTurkeyDataDir(stDataDir);
		factory = new SixDigitCounterURIGeneratorFactory();
		urigen = factory.createInstance();
	}

	void configureURIGenerator(STServiceContext stServiceContext)
			throws STPropertyAccessException, STPropertyUpdateException {
		configureURIGenerator(stServiceContext, 0L, 0L, 0L, 0L);
	}

	void configureURIGenerator(STServiceContext stServiceContext, long seedDefault, long seedConcept,
			long seedXLabel, long seedConceptScheme)
			throws STPropertyAccessException, STPropertyUpdateException {
		SixDigitCounterURIGeneratorProjectSettings projectSettings = factory
				.getProjectSettings(stServiceContext.getProject());
		projectSettings.counter2nextIdentifier = new HashMap<>();
		projectSettings.counter2nextIdentifier.put(SixDigitCounterURIGeneratorProjectSettings.DEFAULT_COUNTER,
				seedDefault);
		projectSettings.counter2nextIdentifier.put(URIGenerator.Roles.concept, seedConcept);
		projectSettings.counter2nextIdentifier.put(URIGenerator.Roles.xLabel, seedXLabel);
		projectSettings.counter2nextIdentifier.put(URIGenerator.Roles.conceptScheme, seedConceptScheme);

		projectSettings.templates = new HashMap<>();
		projectSettings.templates.put(URIGenerator.Roles.concept, "c_${seq(concept)}");
		projectSettings.templates.put(URIGenerator.Roles.xLabel, "xl_${lexicalForm.language}_${seq(xLabel)}");
		projectSettings.templates.put(URIGenerator.Roles.conceptScheme,
				"conceptScheme_${seq(conceptScheme)}");
		projectSettings.templates.put(SixDigitCounterURIGeneratorProjectSettings.FALLBACK_XROLE,
				"${xRole}_${seq()}");

		factory.storeProjectSettings(stServiceContext.getProject(), projectSettings);
	}

	void configureURIGeneratorTemplate(STServiceContext stServiceContext, String xRole, String template)
			throws STPropertyAccessException, STPropertyUpdateException {
		SixDigitCounterURIGeneratorProjectSettings projectSettings = factory
				.getProjectSettings(stServiceContext.getProject());

		if (projectSettings.templates == null) {
			projectSettings.templates = new HashMap<>();
		}

		projectSettings.templates.put(xRole, template);

		factory.storeProjectSettings(project, projectSettings);
	}

	void assertNextIdentifier(STServiceContext stServiceContext, long defaultSeed, long conceptSeed,
			long xLabelSeed, long conceptSchemeSeed) throws STPropertyAccessException {
		assertNextIdentifier(stServiceContext, defaultSeed,
				SixDigitCounterURIGeneratorProjectSettings.DEFAULT_COUNTER);
		assertNextIdentifier(stServiceContext, conceptSeed, URIGenerator.Roles.concept);
		assertNextIdentifier(stServiceContext, xLabelSeed, URIGenerator.Roles.xLabel);
		assertNextIdentifier(stServiceContext, conceptSchemeSeed, URIGenerator.Roles.conceptScheme);
	}

	void assertNextIdentifier(STServiceContext stServiceContext, long value, String counterName)
			throws STPropertyAccessException {
		assertThat(factory.getProjectSettings(stServiceContext.getProject()).counter2nextIdentifier
				.get(counterName), Matchers.equalTo(value));
	}

	@AfterEach
	void destroy() {
		try {
			try {
				if (connection != null) {
					connection.close();
				}
			} finally {
				if (repository != null) {
					repository.shutDown();
				}
			}
		} finally {
			TransactionSynchronizationManager.unbindResource(repository);
		}
	}

	/**
	 * Checks that the URI Generators throws an exception if it wasn't configured. The idea is that the URI
	 * generator will not guess the right identifier, but instead ask the project manager.
	 * 
	 * @throws URIGenerationException
	 */
	@Test
	void testRequireInitialConfiguration() throws URIGenerationException {
		URIGenerationException thrownException = assertThrows(URIGenerationException.class, () -> {
			@SuppressWarnings("unused")
			IRI identifier = urigen.generateIRI(stServiceContext, "resource", Collections.emptyMap());
		});
		assertThat(thrownException.getMessage(), stringContainsInOrder("nextIdentifier: Required property shall be non null"));
	}

	/**
	 * Checks that the URI Generator indeed generates sequential 6-digit sequential identifiers
	 * 
	 * @throws URIGenerationException
	 * @throws STPropertyUpdateException
	 * @throws STPropertyAccessException
	 */
	@Test
	void testSequentialGeneration()
			throws URIGenerationException, STPropertyAccessException, STPropertyUpdateException {

		configureURIGenerator(stServiceContext);

		IRI identifier0 = urigen.generateIRI(stServiceContext, "resource", Collections.emptyMap());
		IRI identifier1 = urigen.generateIRI(stServiceContext, "resource", Collections.emptyMap());
		IRI identifier2 = urigen.generateIRI(stServiceContext, "resource", Collections.emptyMap());

		assertAll(() -> assertThat(identifier0, equalTo(iri(NS, "resource_000000"))),
				() -> assertThat(identifier1, equalTo(iri(NS, "resource_000001"))),
				() -> assertThat(identifier2, equalTo(iri(NS, "resource_000002"))));

		assertNextIdentifier(stServiceContext, 3L, 0L, 0L, 0L);
	}

	/**
	 * Checks that the default (test) configuration produces two consecutive IRIs for concepts, the local name
	 * of which is c_000000 and c_000001.
	 * 
	 * @throws URIGenerationException
	 * @throws STPropertyAccessException
	 * @throws STPropertyUpdateException
	 */
	@Test
	void testDefaultConceptTemplate()
			throws URIGenerationException, STPropertyAccessException, STPropertyUpdateException {
		configureURIGenerator(stServiceContext);

		Map<String, Value> args = new HashMap<>();
		args.put("label", SimpleValueFactory.getInstance().createLiteral("dog", "en"));

		IRI iri = urigen.generateIRI(stServiceContext, "concept", args);

		assertThat(iri, equalTo(iri(NS, "c_000000")));

		args = new HashMap<>();
		args.put("label", SimpleValueFactory.getInstance().createLiteral("cat", "en"));

		iri = urigen.generateIRI(stServiceContext, "concept", args);

		assertThat(iri, equalTo(iri(NS, "c_000001")));

		assertNextIdentifier(stServiceContext, 0, 2L, 0L, 0L);
	}

	/**
	 * Checks that the default (test) configuration produces two consecutive IRIs for eXtended Labels, the
	 * local name of which is xl_en_000000 and xl_en_000001.
	 * 
	 * @throws URIGenerationException
	 * @throws STPropertyAccessException
	 * @throws STPropertyUpdateException
	 */
	@Test
	void testDefaultXLabelTemplate()
			throws URIGenerationException, STPropertyAccessException, STPropertyUpdateException {
		configureURIGenerator(stServiceContext);

		Map<String, Value> args = new HashMap<>();
		args.put("lexicalForm", SimpleValueFactory.getInstance().createLiteral("dog", "en"));

		IRI iri = urigen.generateIRI(stServiceContext, "xLabel", args);

		assertThat(iri, equalTo(iri(NS, "xl_en_000000")));

		args = new HashMap<>();
		args.put("lexicalForm", SimpleValueFactory.getInstance().createLiteral("cat", "en"));

		iri = urigen.generateIRI(stServiceContext, "xLabel", args);

		assertThat(iri, equalTo(iri(NS, "xl_en_000001")));

		assertNextIdentifier(stServiceContext, 0, 0, 2L, 0L);
	}

	/**
	 * Checks that the default (test) configuration produces two consecutive IRIs for concept schemes, the
	 * local name of which is conceptScheme_000000 and conceptScheme_000001.
	 * 
	 * @throws URIGenerationException
	 * @throws STPropertyAccessException
	 * @throws STPropertyUpdateException
	 */
	@Test
	void testDefaultConceptSchemeTemplate()
			throws URIGenerationException, STPropertyAccessException, STPropertyUpdateException {
		configureURIGenerator(stServiceContext);

		Map<String, Value> args = new HashMap<>();

		IRI iri = urigen.generateIRI(stServiceContext, "conceptScheme", args);

		assertThat(iri, equalTo(iri(NS, "conceptScheme_000000")));

		iri = urigen.generateIRI(stServiceContext, "conceptScheme", args);

		assertThat(iri, equalTo(iri(NS, "conceptScheme_000001")));

		assertNextIdentifier(stServiceContext, 0L, 0L, 0L, 2L);
	}

	/**
	 * Checks that the invocation of different counters really increments different counters.
	 * 
	 * @throws URIGenerationException
	 * @throws STPropertyAccessException
	 * @throws STPropertyUpdateException
	 */
	@Test
	void testMultipleCountersUsage()
			throws URIGenerationException, STPropertyAccessException, STPropertyUpdateException {
		configureURIGenerator(stServiceContext);

		{
			Map<String, Value> args = new HashMap<>();
			@SuppressWarnings("unused")
			IRI iri = urigen.generateIRI(stServiceContext, "resource", args);
		}

		{
			Map<String, Value> args = new HashMap<>();
			args.put("label", SimpleValueFactory.getInstance().createLiteral("mouse", "en"));
			@SuppressWarnings("unused")
			IRI iri = urigen.generateIRI(stServiceContext, "concept", args);
		}

		{
			Map<String, Value> args = new HashMap<>();
			args.put("label", SimpleValueFactory.getInstance().createLiteral("cheese", "en"));
			@SuppressWarnings("unused")
			IRI iri = urigen.generateIRI(stServiceContext, "concept", args);
		}

		{
			Map<String, Value> args = new HashMap<>();
			args.put("lexicalForm", SimpleValueFactory.getInstance().createLiteral("dog", "en"));
			@SuppressWarnings("unused")
			IRI iri = urigen.generateIRI(stServiceContext, "xLabel", args);
		}

		{
			Map<String, Value> args = new HashMap<>();
			args.put("lexicalForm", SimpleValueFactory.getInstance().createLiteral("cat", "en"));
			@SuppressWarnings("unused")
			IRI iri = urigen.generateIRI(stServiceContext, "xLabel", args);
		}

		{
			Map<String, Value> args = new HashMap<>();
			args.put("lexicalForm", SimpleValueFactory.getInstance().createLiteral("rabbit", "en"));
			@SuppressWarnings("unused")
			IRI iri = urigen.generateIRI(stServiceContext, "xLabel", args);
		}

		assertNextIdentifier(stServiceContext, 1, 2, 3, 0);
	}

	/**
	 * Checks that it can generate the identifier 999999 (the biggest 6-digit identifier), but a subsequent
	 * attempt to generate an identifier throws an exception, because 1000000 is 7-digit wide.
	 * 
	 * @throws STPropertyAccessException
	 * @throws STPropertyUpdateException
	 * @throws URIGenerationException
	 */
	@Test
	void testUpperbound()
			throws STPropertyAccessException, STPropertyUpdateException, URIGenerationException {
		configureURIGenerator(stServiceContext, 999999L, 0L, 0L, 0L);

		@SuppressWarnings("unused")
		IRI identifier999999 = urigen.generateIRI(stServiceContext, "resource", Collections.emptyMap());

		URIGenerationException thrownException = assertThrows(URIGenerationException.class, () -> {
			@SuppressWarnings("unused")
			IRI identifier1000000 = urigen.generateIRI(stServiceContext, "resource", Collections.emptyMap());
		});
		assertThat(thrownException.getMessage(),
				stringContainsInOrder("can't be represented using six digits"));
		assertNextIdentifier(stServiceContext, 1000000L, 0L, 0L, 0L);
	}

	/**
	 * Checks that the URI Generator throws an exception, if the generated URI is already in use.
	 * 
	 * @throws STPropertyAccessException
	 * @throws STPropertyUpdateException
	 */
	@Test
	void testIdentifierReuseAvoidance() throws STPropertyAccessException, STPropertyUpdateException {
		Repositories.consume(repository,
				conn -> conn.add(conn.getValueFactory().createIRI(NAMESPACE, "resource_000000"), RDF.TYPE,
						SKOS.CONCEPT, conn.getValueFactory().createIRI(BASEURI)));

		configureURIGenerator(stServiceContext, 0L, 0L, 0L, 0L);

		URIGenerationException thrownException = assertThrows(URIGenerationException.class,
				() -> urigen.generateIRI(stServiceContext, "resource", Collections.emptyMap()));
		assertThat(thrownException.getMessage(), stringContainsInOrder("already in use"));

	}

	/**
	 * Checks that sanitization is performed: 1) consecutive spaces are replaced with a single underscore, 2)
	 * percent-encoding is applied
	 * 
	 * @throws URIGenerationException
	 * @throws STPropertyAccessException
	 * @throws STPropertyUpdateException
	 */
	@Test
	void testSpaceEscapingAndPercentEncoding()
			throws URIGenerationException, STPropertyAccessException, STPropertyUpdateException {
		configureURIGenerator(stServiceContext, 0L, 0L, 0L, 0L);
		configureURIGeneratorTemplate(stServiceContext, URIGenerator.Roles.concept,
				"c_${label.language}_${label}_${seq(concept)}");
		Map<String, Value> args = new HashMap<>();
		args.put("label", SimpleValueFactory.getInstance().createLiteral("Questa  è  Sparta", "it")); // note:
																										// double
																										// spacing
		IRI iri = urigen.generateIRI(stServiceContext, URIGenerator.Roles.concept, args);
		assertThat(iri, equalTo(iri(NS, "c_it_Questa_%C3%A8_Sparta_000000")));
	}

	/**
	 * Checks the behavior of the URI Generator with respect to concurrent attempts to generate new IRIs. This
	 * test case. Without a proper locking strategy, the test may fail because of two reasons:
	 * <ul>
	 * <li>invalid settings file (probably a corruption due to simultaneous writes or a read/write race)</li>
	 * <li>two different threads have generated some identifiers in common</li>
	 * </ul>
	 * 
	 * @param sleepFactor
	 * @throws STPropertyAccessException
	 * @throws STPropertyUpdateException
	 * @throws InterruptedException
	 * @throws ExecutionException
	 */
	@ParameterizedTest
	@ValueSource(ints = { 10, 100 })
	void testConcurrentExecution(int sleepFactor) throws STPropertyAccessException, STPropertyUpdateException,
			InterruptedException, ExecutionException {

		// A callable that attempts to generate 100 identifiers
		Function<STServiceContext, Callable<List<IRI>>> generatorCallable = (STServiceContext ctx) -> () -> {
			try (RepositoryConnection conn = repository.getConnection()) {
				conn.setNamespace("", NAMESPACE);
				TransactionSynchronizationManager.bindResource(repository, conn);

				List<IRI> rv = new ArrayList<>();

				for (int i = 0; i < 10; i++) {
					IRI iri = urigen.generateIRI(ctx, "resource", Collections.emptyMap());
					Thread.sleep((int) (sleepFactor * Math.random()));
					rv.add(iri);
				}

				return rv;
			} finally {
				TransactionSynchronizationManager.unbindResource(repository);
			}
		};

		// here I can't use the singleton mocks created by the fixture, because I need two instances
		Project project = mock(Project.class);
		STServiceContext stServiceContext1 = mock(STServiceContext.class);
		STServiceContext stServiceContext2 = mock(STServiceContext.class);

		when(project.getRepository()).thenReturn(repository);
		when(project.getName()).thenReturn(TEST_PROJECT_NAME);
		when(stServiceContext1.getProject()).thenReturn(project);
		when(stServiceContext2.getProject()).thenReturn(project);

		configureURIGenerator(stServiceContext1, 0, 0, 0, 0);

		// Execute the the callable twice
		ExecutorService executor = Executors.newFixedThreadPool(2);
		List<Future<List<IRI>>> futuresList = executor.invokeAll(Arrays.asList(
				generatorCallable.apply(stServiceContext1), generatorCallable.apply(stServiceContext2)));

		// Waits that all tasks have completed
		executor.shutdown();
		executor.awaitTermination(60, TimeUnit.SECONDS);

		Future<List<IRI>> future1 = futuresList.get(0);
		Future<List<IRI>> future2 = futuresList.get(1);

		List<IRI> list1 = future1.get();
		List<IRI> list2 = future2.get();

		// Turns each list of identifiers into a set
		Set<IRI> set1 = new HashSet<>(list1);
		Set<IRI> set2 = new HashSet<>(list2);

		// Checks that the identifiers in each list are unique within the list (i.e. the set and the list have
		// the same size)
		assertAll(() -> assertThat(list1.size(), equalTo(set1.size())),
				() -> assertThat(list2.size(), equalTo(set2.size())));

		// There is no IRI in common between the two sequences
		assertThat(Sets.intersection(set1, set2), empty());

	}

}
