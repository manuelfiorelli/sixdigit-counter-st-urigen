/*
 * Copyright (C) 2018 Manuel Fiorelli <mailto:manuel.fiorelli@gmail.com>
 * All rights reserved.
 * This software may be modified and distributed under the terms of the BSD-3-Clause license.
 * See the LICENSE.txt file for details.
 */
package org.bitbucket.manuelfiorelli.semanticturkey.sixdigitcounterurigen;

import java.util.Map;

import it.uniroma2.art.semanticturkey.extension.settings.Settings;
import it.uniroma2.art.semanticturkey.properties.Required;
import it.uniroma2.art.semanticturkey.properties.STProperty;

/**
 * Settings class for {@link SixDigitCounterURIGenerator}.
 * 
 * @author <a href="mailto:manuel.fiorelli@gmail.com">Manuel Fiorelli</a>
 *
 */
public class SixDigitCounterURIGeneratorProjectSettings implements Settings {

	/**
	 * Virtual eXtended Role that can be used to indicate a fallback template
	 */
	public static final String FALLBACK_XROLE = "fallback";

	/**
	 * Name of the counter that is used if none is combined with the {@cod seq} placeholder
	 */
	public static final String DEFAULT_COUNTER = "default";

	@Override
	public String getShortName() {
		return "\"6-digit Counter URI Generator Settings";
	}

	@STProperty(description = "Associates a named counter to its next identifier")
	@Required
	public Map<String, Long> counter2nextIdentifier;

	@STProperty(description = "Templates to be used for instantiating the local name of resources")
	@Required
	public Map<String, String> templates;

}
