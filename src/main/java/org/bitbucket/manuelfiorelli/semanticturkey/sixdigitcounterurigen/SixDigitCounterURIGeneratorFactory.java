/*
 * Copyright (C) 2018 Manuel Fiorelli <mailto:manuel.fiorelli@gmail.com>
 * All rights reserved.
 * This software may be modified and distributed under the terms of the BSD-3-Clause license.
 * See the LICENSE.txt file for details.
 */
package org.bitbucket.manuelfiorelli.semanticturkey.sixdigitcounterurigen;

import it.uniroma2.art.semanticturkey.extension.ExtensionFactory;
import it.uniroma2.art.semanticturkey.extension.NonConfigurableExtensionFactory;
import it.uniroma2.art.semanticturkey.extension.settings.ProjectSettingsManager;
import org.springframework.stereotype.Component;

/**
 * The {@link ExtensionFactory} for {@link SixDigitCounterURIGenerator}. This factory is in charge of storing
 * the sequential counters as project settings (see {@link ProjectSettingsManager}).
 * 
 * @author <a href="mailto:manuel.fiorelli@gmail.com">Manuel Fiorelli</a>
 *
 */
@Component
public class SixDigitCounterURIGeneratorFactory
		implements NonConfigurableExtensionFactory<SixDigitCounterURIGenerator>,
		ProjectSettingsManager<SixDigitCounterURIGeneratorProjectSettings> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.semanticturkey.extension.ExtensionFactory#getName()
	 */
	@Override
	public String getName() {
		return "6-digit Counter URI Generator";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.semanticturkey.extension.ExtensionFactory#getDescription()
	 */
	@Override
	public String getDescription() {
		return "A URI generator that uses six-digit sequential counters";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.semanticturkey.extension.NonConfigurableExtensionFactory#createInstance()
	 */
	@Override
	public SixDigitCounterURIGenerator createInstance() {
		return new SixDigitCounterURIGenerator(this);
	}

}
