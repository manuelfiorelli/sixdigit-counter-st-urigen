/*
 * Copyright (C) 2018 Manuel Fiorelli <mailto:manuel.fiorelli@gmail.com>
 * All rights reserved.
 * This software may be modified and distributed under the terms of the BSD-3-Clause license.
 * See the LICENSE.txt file for details.
 */
package org.bitbucket.manuelfiorelli.semanticturkey.sixdigitcounterurigen;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URLEncoder;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;

import it.uniroma2.art.semanticturkey.extension.extpts.urigen.URIGenerationException;
import it.uniroma2.art.semanticturkey.extension.extpts.urigen.URIGenerator;
import it.uniroma2.art.semanticturkey.extension.settings.ProjectSettingsManager;
import it.uniroma2.art.semanticturkey.properties.STPropertiesChecker;
import it.uniroma2.art.semanticturkey.properties.STPropertyAccessException;
import it.uniroma2.art.semanticturkey.properties.STPropertyUpdateException;
import it.uniroma2.art.semanticturkey.services.STServiceContext;
import it.uniroma2.art.semanticturkey.tx.RDF4JRepositoryUtils;

/**
 * A {@link URIGenerator} that uses six-digit sequentially incremented counters. This component stores its
 * counters as project settings (see {@link ProjectSettingsManager}).
 * 
 * @author <a href="mailto:manuel.fiorelli@gmail.com">Manuel Fiorelli</a>
 *
 */
public class SixDigitCounterURIGenerator implements URIGenerator {

	/**
	 * The width of the identifiers
	 */
	private static final int IDENTIFIER_WIDTH = 6;

	/**
	 * Six-digit numeric identifiers should be less than 1 million
	 */
	private static final long IDENTIFIER_UPPERBOUND = 1000000;

	/**
	 * A format string for {@link String#format(String, Object...)} that produces identifiers padded with
	 * zeroes on the left
	 */
	private static final String LOCAL_NAME_FORMAT = "%0" + IDENTIFIER_WIDTH + "d";

	/**
	 * Patterns for the identification of placeholders inside a template for the generation of local names
	 */
	private static final Pattern PLACEHOLDER_PATTERN = Pattern.compile("\\$\\{(.+?)\\}");

	/**
	 * Pattern for the invocation of functions inside a placeholder, e.g. ${seq(concept)}
	 */
	private static final String FUNCTION_INVOCATION_PATTERN = "(?<functor>[a-zA-z][a-zA-Z0-9_]*)\\((?<arg>[a-zA-z][a-zA-Z0-9_]*)?\\)";

	/**
	 * Pattern for the use of a variable plus an optional property inside a placeholder, e.g
	 * ${lexicalForm.language}
	 */
	private static final String VARIABLE_PLUS_PROPERTY_PATTERN = "(?<variable>[a-zA-z][a-zA-Z0-9_]*)(\\.(?<property>[a-zA-z][a-zA-Z0-9_]*))?";

	/**
	 * Pattern for parsing the content of a placeholder
	 */
	private static final Pattern PLACEHOLDER_CONTENT_PATTERN = Pattern
			.compile("(" + FUNCTION_INVOCATION_PATTERN + ")|(" + VARIABLE_PLUS_PROPERTY_PATTERN + ")");

	/**
	 * The variable for the eXtended Role
	 */
	private static final String XROLE_VARIABLE = "xRole";

	/**
	 * The placeholder for a sequential counter
	 */
	private static final String SEQ_PLACEHOLDER = "seq";

	/**
	 * Reference to the extension factory
	 */
	private SixDigitCounterURIGeneratorFactory factory;

	public SixDigitCounterURIGenerator(SixDigitCounterURIGeneratorFactory factory) {
		this.factory = factory;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.uniroma2.art.semanticturkey.extension.extpts.urigen.URIGenerator#generateIRI(it.uniroma2.art.
	 * semanticturkey.services.STServiceContext, java.lang.String, java.util.Map)
	 */
	@Override
	public synchronized IRI generateIRI(STServiceContext stServiceContext, String xRole,
			Map<String, Value> args) throws URIGenerationException {

		// Obtains a reference to the connection bound to the current transaction
		Repository repo = stServiceContext.getProject().getRepository();

		// Notice: we do not close this connection explicitly, because it is closed by the transaction manager
		// of Semantic Turkey
		RepositoryConnection conn = RDF4JRepositoryUtils.getConnection(repo, false);

		// Obtains the default namespace
		String defaultNamespace = Objects.requireNonNull(conn.getNamespace(""),
				"Default namespace not specified for the repository");

		// Retrieves and validates project settings
		SixDigitCounterURIGeneratorProjectSettings projectSettings;
		try {
			projectSettings = factory.getProjectSettings(stServiceContext.getProject());
		} catch (STPropertyAccessException e) {
			throw new URIGenerationException("Unable to load the counter", e);
		}

		STPropertiesChecker settingsChecker = STPropertiesChecker
				.getModelConfigurationChecker(projectSettings);
		if (!settingsChecker.isValid()) {
			throw new URIGenerationException("Project settings for the URI Generator are not valid: "
					+ settingsChecker.getErrorMessage());
		}

		String localName = computeLocalName(projectSettings, xRole, args);

		// Generates the new IRI
		IRI iri = conn.getValueFactory().createIRI(defaultNamespace, localName);

		// Checks that the IRI is not already in use
		if (conn.hasStatement(iri, null, null, true) || conn.hasStatement(null, iri, null, true)
				|| conn.hasStatement(null, null, iri, true)) {
			throw new URIGenerationException("The generated IRI is already in use: " + iri);
		}

		// Stores the updated settings
		try {
			factory.storeProjectSettings(stServiceContext.getProject(), projectSettings);
		} catch (STPropertyUpdateException e) {
			throw new URIGenerationException("Unable to store the incremented counter", e);
		}

		// Returns the newly minted IRI
		return iri;
	}

	/**
	 * Computes the local name of the resource
	 * 
	 * @param projectSettings
	 * @param xRole
	 * @param args
	 * @return
	 * @throws URIGenerationException
	 */
	protected String computeLocalName(SixDigitCounterURIGeneratorProjectSettings projectSettings,
			String xRole, Map<String, Value> args) throws URIGenerationException {

		// Locates a suitable template for the local name
		String template = projectSettings.templates.get(xRole);

		if (template == null) {
			template = projectSettings.templates
					.get(SixDigitCounterURIGeneratorProjectSettings.FALLBACK_XROLE);
		}

		if (template == null) {
			throw new URIGenerationException("Unable to find a template for xRole " + xRole);
		}

		// Instantiates the placeholders contained in the template
		Matcher placeholderMatcher = PLACEHOLDER_PATTERN.matcher(template);
		StringBuffer sb = new StringBuffer();

		while (placeholderMatcher.find()) {

			String placeholderContent = placeholderMatcher.group(1);

			Matcher placeholderParsingMatcher = PLACEHOLDER_CONTENT_PATTERN.matcher(placeholderContent);

			if (!placeholderParsingMatcher.matches()) {
				throw new URIGenerationException("Invalid placeholder content: " + placeholderContent);
			}

			// ${seq(concept)}
			String functor = placeholderParsingMatcher.group("functor");
			String arg = placeholderParsingMatcher.group("arg");

			// ${lexicalForm.language}
			String variable = placeholderParsingMatcher.group("variable");

			String property = placeholderParsingMatcher.group("property");
			String replacement;

			if (functor != null) {
				if (Objects.equals(functor, SEQ_PLACEHOLDER)) {
					// ${seq(counterName)} --> counter = counterName or ${seq} --> counter = default
					String counterName = arg != null ? arg
							: SixDigitCounterURIGeneratorProjectSettings.DEFAULT_COUNTER;

					Long numericIdentifier = projectSettings.counter2nextIdentifier.get(counterName);
					if (numericIdentifier == null) {
						throw new URIGenerationException("Counter not initialized: " + counterName);
					}

					if (numericIdentifier < 0) {
						throw new URIGenerationException("Current numeric identifier for counter \""
								+ counterName + "\" is negative: " + numericIdentifier);
					} else if (numericIdentifier >= IDENTIFIER_UPPERBOUND) {
						throw new URIGenerationException(
								"Current numeric identifier for counter \"" + counterName
										+ "\"can't be represented using six digits: " + numericIdentifier);
					}

					replacement = String.format(LOCAL_NAME_FORMAT, numericIdentifier);

					// Increments the counter
					long nextIdentifier = Math.incrementExact(numericIdentifier);
					projectSettings.counter2nextIdentifier.put(counterName, nextIdentifier);
				} else {
					throw new URIGenerationException("Unhandled functor: \"" + functor
							+ "\" inside placeholder: " + placeholderMatcher.group());
				}
			} else if (variable != null) {
				Object obj;
				// obtains the value of the variable
				if (Objects.equals(variable, XROLE_VARIABLE)) { // special xRole variable
					obj = xRole;
				} else { // variable associated with an argument of the URIGenerator
					obj = args.get(variable);
				}

				if (obj == null) {
					throw new URIGenerationException("Uninitialized variable \"" + variable
							+ "\" inside placeholder: " + placeholderMatcher.group());
				}

				// If provided, dereferences the property
				if (property != null) {
					obj = accessProperty(obj, property);
				}

				replacement = stringify(obj);

				if (replacement == null) {
					throw new URIGenerationException(
							"Placeholder with null value: " + placeholderMatcher.group());
				}

				// Replaces spaces with _
				replacement = replacement.replaceAll("\\s+", "_");

				try {
					replacement = URLEncoder.encode(replacement, "UTF-8");
				} catch (UnsupportedEncodingException e) {
					throw new URIGenerationException(e); // this should not happen
				}
			} else {
				throw new URIGenerationException(
						"Unhandled placeholder content: " + placeholderMatcher.group()); // this exception
																							// should
				// never be thrown
			}

			placeholderMatcher.appendReplacement(sb, replacement);
		}

		placeholderMatcher.appendTail(sb);

		return sb.toString();
	}

	/**
	 * Returns a string representation of the provided object.
	 * 
	 * @param obj
	 * @return
	 */
	protected String stringify(Object obj) {
		if (obj instanceof Optional) {
			obj = ((Optional<?>) obj).orElse(null);
		}

		if (obj == null)
			return null;
		if (obj instanceof Value) {
			return ((Value) obj).stringValue();
		}
		return obj.toString();
	}

	/**
	 * Accesses a property of an Object. For security reasons, it is only possible to invoke methods defined
	 * by RDF4J's {@link Value}s.
	 * 
	 * @param value
	 * @param property
	 * @return
	 * @throws URIGenerationException
	 */
	protected Object accessProperty(Object value, String property) throws URIGenerationException {
		if (value instanceof Value) {
			String methodName = "get" + StringUtils.capitalize(property);
			try {
				Method m = value.getClass().getMethod(methodName);
				return m.invoke(value);
			} catch (NoSuchMethodException | SecurityException | IllegalAccessException
					| IllegalArgumentException | InvocationTargetException e) {
				throw new URIGenerationException(e);
			}
		} else {
			throw new URIGenerationException(
					"Forbidden property dereferenciation on object of class " + value.getClass());
		}
	}
}
