package org.bitbucket.manuelfiorelli.semanticturkey.sixdigitcounterurigen;

import it.uniroma2.art.semanticturkey.pf4j.STPlugin;
import org.pf4j.PluginWrapper;

public class SixDigitCounterURIGeneratorPlugin extends STPlugin {
	
    public SixDigitCounterURIGeneratorPlugin(PluginWrapper wrapper) {
        super(wrapper);
    }
    
    @Override
    public void start()  {
        // This method is called by the application when the plugin is started.
    	log.info("SixDigitCounterURIGeneratorPlugin.start()");
    }
}
