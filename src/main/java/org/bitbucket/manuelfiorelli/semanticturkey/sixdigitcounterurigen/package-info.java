/*
 * Copyright (C) 2018 Manuel Fiorelli <mailto:manuel.fiorelli@gmail.com>
 * All rights reserved.
 * This software may be modified and distributed under the terms of the BSD-3-Clause license.
 * See the LICENSE.txt file for details.
 */
/**
 * Main implementation package of the 6-digit counter URI Generator
 * 
 * @author <a href="mailto:manuel.fiorelli@gmail.com">Manuel Fiorelli</a>
 *
 */
package org.bitbucket.manuelfiorelli.semanticturkey.sixdigitcounterurigen;