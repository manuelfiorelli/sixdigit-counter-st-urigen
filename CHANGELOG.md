[4.0](https://bitbucket.org/manuelfiorelli/sixdigit-counter-st-urigen/src/4.0/) (2024-12-03)
============================================================================================

Release required to make this URIGenerator compatible with Semantic Turkey 13.x.
Thanks to Saku Seppälä for the contribution!

Updated dependencies
--------------------

**compile dependencies**

* Semantic Turkey: 13.0

New dependencies
--------------------

**test dependencies**

* spring-boot-starter-validation: 3.2.5

**build Plugins**

* maven.jar.plugin.version: 3.4.2

Compatibility
-------------
Semantic Turkey 13.x (this version of the URIGenerator is backward compatible with the settings stored by the
previous version).

The plugin now requires Java 17 or greater.

[3.0](https://bitbucket.org/manuelfiorelli/sixdigit-counter-st-urigen/src/3.0/) (2019-09-04)
============================================================================================

Release required to make this URIGenerator compatible with Semantic Turkey 6.x

Updated dependencies
--------------------

**compile dependencies**

 * Semantic Turkey: 6.0
 
Compatibility
-------------
Semantic Turkey 6.x (this version of the URIGenerator is backward compatible with the settings stored by the
previous version)

[2.0](https://bitbucket.org/manuelfiorelli/sixdigit-counter-st-urigen/src/2.0/) (2019-03-31)
============================================================================================

Release required to make this URIGenerator compatible with Semantic Turkey 5.x

Updated dependencies
--------------------

**compile dependencies**

 * Semantic Turkey: 5.0

**test dependencies**

 * JUnit Platform: 1.4.1
 * JUnit Jupiter: 5.4.1
 * Mockito: 2.25.1

**build Plugins**
 
 * Maven Compiler Plugin: 3.8.0
 * Maven Surefire Plugin: 2.22.1
 * Maven Bundle Plugin: 4.1.0

Compatibility
-------------
Semantic Turkey 5.x (this version of the URIGenerator is backward compatible with the settings stored by the
previous version)

[1.0](https://bitbucket.org/manuelfiorelli/sixdigit-counter-st-urigen/src/1.0/) (2019-03-30)
============================================================================================

Initial release

Compatibility
-------------
Semantic Turkey 4.x